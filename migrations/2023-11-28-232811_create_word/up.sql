-- Your SQL goes here
CREATE TABLE words (
  id SERIAL PRIMARY KEY,
  line_id INTEGER NOT NULL REFERENCES lines(id),
  content TEXT NOT NULL,
  start_time FLOAT NOT NULL,
  end_time FLOAT NOT NULL
)
