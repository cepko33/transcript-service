ALTER TABLE transcripts
DROP COLUMN subtitle;

ALTER TABLE transcripts
DROP COLUMN description;

ALTER TABLE transcripts
DROP COLUMN source_url;
