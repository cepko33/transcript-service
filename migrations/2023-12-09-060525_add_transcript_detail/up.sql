-- Your SQL goes here
ALTER TABLE transcripts
ADD COLUMN subtitle TEXT;

ALTER TABLE transcripts
ADD COLUMN description TEXT;

ALTER TABLE transcripts
ADD COLUMN source_url TEXT;
