-- This file should undo anything in `up.sql`
ALTER TABLE transcripts
DROP COLUMN header_photo_url;

ALTER TABLE lines
DROP COLUMN attached_photo_url;
