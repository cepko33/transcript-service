-- Your SQL goes here
ALTER TABLE transcripts
ADD COLUMN header_photo_url TEXT;

ALTER TABLE lines
ADD COLUMN attached_photo_url TEXT;
