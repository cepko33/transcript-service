-- Your SQL goes here
CREATE TABLE users (
  id SERIAL PRIMARY KEY,
  email VARCHAR(100) NOT NULL,
  password VARCHAR(122) NOT NULL, --argon hash
  CONSTRAINT email_unique_constraint UNIQUE (email)
);
