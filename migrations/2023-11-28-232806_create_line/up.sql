-- Your SQL goes here
CREATE TABLE lines (
  id SERIAL PRIMARY KEY,
  transcript_id INTEGER NOT NULL REFERENCES transcripts(id)
)
