-- This file should undo anything in `up.sql`

ALTER TABLE lines
DROP COLUMN speaker_id;

DROP TABLE speakers;
