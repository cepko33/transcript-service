-- Your SQL goes here
CREATE TABLE speakers (
  id SERIAL PRIMARY KEY,
  name TEXT NOT NULL,
  color TEXT NOT NULL
);

ALTER TABLE lines
ADD COLUMN speaker_id INTEGER REFERENCES speakers(id);
