CREATE TABLE shows (
  id SERIAL PRIMARY KEY,
  name TEXT NOT NULL,
  rss_url TEXT,
  description TEXT,
  link TEXT,
  image TEXT,
  author TEXT
);

ALTER TABLE transcripts
ADD COLUMN show_id INTEGER REFERENCES shows(id);

ALTER TABLE transcripts
ADD COLUMN episode_number INTEGER;