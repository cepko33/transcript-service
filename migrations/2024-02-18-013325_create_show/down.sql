-- This file should undo anything in `up.sql`
ALTER TABLE transcripts
DROP COLUMN show_id;

ALTER TABLE transcripts
DROP COLUMN episode_number;

DROP TABLE shows;