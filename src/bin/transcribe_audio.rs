#![allow(clippy::uninlined_format_args)]

use core::ffi::{c_void, CStr};
use hound::{SampleFormat, WavReader};
use podcast::models::*;
use std::path::Path;
use std::process::Command;
use whisper_rs::{
    FullParams, SamplingStrategy, WhisperContext, WhisperContextParameters, WhisperSysContext,
    WhisperSysState,
};

fn parse_wav_file(path: &Path) -> Vec<i16> {
    let reader = WavReader::open(path).expect("failed to read file");

    if reader.spec().channels != 1 {
        panic!("expected mono audio file");
    }
    if reader.spec().sample_format != SampleFormat::Int {
        panic!("expected integer sample format");
    }
    if reader.spec().sample_rate != 16000 {
        panic!("expected 16KHz sample rate");
    }
    if reader.spec().bits_per_sample != 16 {
        panic!("expected 16 bits per sample");
    }

    reader
        .into_samples::<i16>()
        .map(|x| x.expect("sample"))
        .collect::<Vec<_>>()
}

fn download_from_url_and_convert(url: String) {
    Command::new("wget")
        .arg("-O")
        .arg("/tmp/temp_audio_file")
        .arg(url);

    Command::new("ffmpeg")
        .arg("-i")
        .arg("/tmp/temp_audio_file")
        .arg("-ac")
        .arg("1")
        .arg("-ar")
        .arg("16000")
        .arg("-c:a")
        .arg("pcm_s16le")
        .arg("/tmp/temp_audio_file.wav")
        .arg("-y")
        .output()
        .expect("Failed to convert");
}

struct UDATA {
    pub t_id: i32,
    pub l_id: i32,
}

fn main() {
    let arg1 = std::env::args()
        .nth(1)
        .expect("first argument should be an audio URL");

    download_from_url_and_convert(arg1);

    let audio_path = Path::new("/tmp/temp_audio_file.wav");
    if !audio_path.exists() {
        panic!("audio file doesn't exist");
    }

    let arg2 = std::env::args()
        .nth(2)
        .expect("second argument should be path to Whisper model");
    let whisper_path = Path::new(&arg2);
    if !whisper_path.exists() {
        panic!("whisper file doesn't exist")
    }

    transcribe_with_whisper(audio_path, whisper_path);
}

unsafe extern "C" fn seg_callback(
    c: *mut WhisperSysContext,
    s: *mut WhisperSysState,
    _n: i32,
    v: *mut c_void,
) {
    let user_data = &mut *(v as *mut UDATA);
    let segment_id = whisper_rs_sys::whisper_full_n_segments_from_state(s);
    let num_tokens = whisper_rs_sys::whisper_full_n_tokens_from_state(s, segment_id - 1);
    let mut current_line: Line;
    if (*user_data).l_id == 0 {
        current_line = Line::new((*user_data).t_id, None).expect("Line error");
        (*user_data).l_id = current_line.id;
    }

    for j in 0..num_tokens {
        let wtd: whisper_rs_sys::whisper_token_data =
            whisper_rs_sys::whisper_full_get_token_data_from_state(s, segment_id - 1, j);
        let token_text =
            whisper_rs_sys::whisper_full_get_token_text_from_state(c, s, segment_id - 1, j);
        let c_str = CStr::from_ptr(token_text);
        let r_str = c_str.to_string_lossy();
        println!("{:?} - {:?} : {:?}", wtd.t0, wtd.t1, r_str.trim_start());

        if r_str.eq("[_BEG_]") {
            current_line = Line::new((*user_data).t_id, None).expect("line error");
            (*user_data).l_id = current_line.id;
        } else if j < (num_tokens - 1) {
            Word::new(
                (*user_data).l_id,
                r_str.trim_start(),
                (wtd.t0 as f64) / 100_f64,
                (wtd.t1 as f64) / 100_f64,
            )
            .expect("Word error");
        }
    }
}

fn transcribe_with_whisper(audio_path: &Path, whisper_path: &Path) {
    println!("{:?}", audio_path);
    let original_samples = parse_wav_file(audio_path);
    let samples = whisper_rs::convert_integer_to_float_audio(&original_samples);

    let ctx = WhisperContext::new_with_params(
        &whisper_path.to_string_lossy(),
        WhisperContextParameters::default(),
    )
    .expect("failed to open model");
    let mut state = ctx.create_state().expect("failed to create key");
    let mut params = FullParams::new(SamplingStrategy::default());
    params.set_initial_prompt("experience");

    let transcript = Transcript::new(TranscriptNew {
        title: Some("Test transcript".to_string()), ..Default::default()
    }).expect("Failed to create transcript");

    let user_data = Box::new(UDATA {
        t_id: transcript.id,
        l_id: 0,
    });
    let ptr = Box::into_raw(user_data) as *mut c_void;

    unsafe {
        params.set_new_segment_callback(Some(seg_callback));
        params.set_new_segment_callback_user_data(ptr);
    }

    params.set_n_threads(8);
    params.set_split_on_word(true);
    params.set_token_timestamps(true);
    params.set_suppress_non_speech_tokens(true);
    params.set_suppress_blank(true);
    params.set_tdrz_enable(true);

    state
        .full(params, &samples)
        .expect("failed to convert samples");
}
