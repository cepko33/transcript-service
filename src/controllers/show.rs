use rocket::post;
use rss::Channel;
use crate::models::{Show, ShowNew, Transcript, TranscriptNew};
use rocket::serde::json::Json;

#[post("/show_from_rss/<link>")]
pub fn make_show_from_rss(link: String) -> Result<Json<(Show, Vec<Transcript>)>, std::io::Error> {
    let mut data = Vec::new();

    {
        let mut easy = curl::easy::Easy::new();
        easy.url(&link)?;

        let mut transfer = easy.transfer();
        transfer.write_function(|new_data| {
            data.extend_from_slice(new_data);
            Ok(new_data.len())
        })?;

        transfer.perform()?;
    }

    let data = String::from_utf8(data).expect("UTf8");

    let channel = Channel::read_from(data.as_bytes()).unwrap();

    let serialized_show = ShowNew {
        name: channel.title,
        rss_url: Some(link),
        description: Some(channel.description),
        image: Some(channel.image.unwrap().url),
        link: Some(channel.link),
        author: channel.copyright,
    };

    let inserted_show = Show::new(serialized_show).unwrap();

    let mut inserted_episodes: Vec<Transcript> = Vec::new();

    for episode in channel.items.into_iter() {
        let itunes_data = episode.itunes_ext.clone().unwrap();
        let serialized_episode = TranscriptNew {
            title: episode.title,
            subtitle: itunes_data.subtitle,
            description: episode.description,
            source_url: Some(episode.enclosure.unwrap().url),
            header_photo_url: itunes_data.image,
            episode_number: Some(itunes_data.episode.unwrap().parse::<i32>().unwrap()),
            show_id: Some(inserted_show.id),
        };

        let inserted_episode = Transcript::new(serialized_episode).unwrap();
        inserted_episodes.push(inserted_episode);
    }

    Ok(Json((inserted_show, inserted_episodes)))
}