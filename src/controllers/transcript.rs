use crate::models::{Line, Speaker, Transcript, Word};
use rocket::serde::json::Json;
use rocket::{delete, get};

#[get("/transcripts")]
pub fn get_all() -> Json<Vec<Transcript>> {
    let retrieved_transcripts = Transcript::get_transcripts();
    retrieved_transcripts
        .map(Json)
        .expect("Error loading Transcripts")
}

#[get("/transcript/<t_id>")]
pub fn get(t_id: i32) -> Json<Transcript> {
    let retrieved_transcript = Transcript::get_by_id(t_id);
    retrieved_transcript
        .map(Json)
        .expect("Error loading Transcript")
}

#[get("/full_transcript/<transcript_id>")]
pub fn get_full(transcript_id: i32) -> Json<(Transcript, Vec<(Line, Vec<Word>)>, Vec<Speaker>)> {
    let retrieved_transcript = Transcript::get_full_transcript(transcript_id);
    retrieved_transcript
        .map(Json)
        .expect("Error loading full transcript")
}

#[delete("/delete_full_transcript/<transcript_id>")]
pub fn delete_full(transcript_id: i32) -> Json<()> {
    let deleted_transcript = Transcript::delete_full_transcript(transcript_id);
    deleted_transcript
        .map(Json)
        .expect("Error loading full transcript")
}
