use crate::controllers::show;

use super::line;
use super::speaker;
use super::transcript;
use super::user;
use super::word;

use rocket::{routes, Route};

pub fn api_routes() -> Vec<Route> {
    routes![
        user::new,
        user::login,
        line::get,
        line::create_update,
        word::get,
        word::create_update,
        word::delete,
        speaker::get_all,
        speaker::get,
        speaker::new,
        speaker::update,
        transcript::get,
        transcript::get_all,
        transcript::get_full,
        transcript::delete_full,
        show::make_show_from_rss,
    ]
}
