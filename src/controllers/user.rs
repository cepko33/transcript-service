use crate::models::auth::create_token;
use crate::models::*;
use rocket::http::Status;
use rocket::post;
use rocket::response::status::Conflict;
use rocket::serde::json::Json;
use serde::{Deserialize, Serialize};

#[derive(Deserialize, Serialize, Debug)]
pub struct Credentials {
    email: String,
    password: String,
}

#[post("/user", format = "json", data = "<new_user>")]
pub fn new(new_user: Json<UserNew>) -> Result<Json<usize>, Conflict<String>> {
    User::new(new_user.into_inner())
        .map(Json)
        .map_err(|_| Conflict("Existing user with email".to_string()))
}

#[post("/login", data = "<credentials>")]
pub fn login(credentials: Json<Credentials>) -> Result<Json<String>, Status> {
    let header = Default::default();
    let email = credentials.email.to_string();
    let password = credentials.password.to_string();

    match User::get_user_with_credentials(&email, &password) {
        Some(user) => match create_token(&user, header) {
            Ok(token) => Ok(Json::<String>(token.as_str().to_string())),
            Err(_) => Err(Status::InternalServerError),
        },
        None => Err(Status::Unauthorized),
    }
}
