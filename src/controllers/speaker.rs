use rocket::serde::json::Json;
use crate::models::{Speaker, SpeakerNew};
use rocket::{post, get};

#[get("/speakers")]
pub fn get_all() -> Json<Vec<Speaker>> {
    let retrieved_speakers = Speaker::get_all();
    retrieved_speakers.map(Json).expect("Error loading Speakers")
}

#[get("/speaker/<speaker_id>")]
pub fn get(speaker_id: i32) -> Json<Speaker> {
    let retrieved_speaker = Speaker::get_by_id(speaker_id);
    retrieved_speaker.map(Json).expect("Error loading Speaker")
}

#[post("/speaker/new", format = "json", data = "<speaker>")]
pub fn new(speaker: Json<SpeakerNew>) -> Json<Speaker> {
    let created_speaker = Speaker::new(&speaker.name, &speaker.color);
    created_speaker.map(Json).expect("Error creating Speaker")
}

#[post("/speaker", format = "json", data = "<speaker>")]
pub fn update(speaker: Json<Speaker>) -> Json<Speaker> {
    let updated_speaker = Speaker::update(speaker);
    updated_speaker.map(Json).expect("Error updating Speaker")
}

