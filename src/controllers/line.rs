use rocket::serde::json::Json;
use crate::models::Line;
use rocket::{get, post};

#[get("/line/<l_id>")]
pub fn get(l_id: i32) -> Json<Line> {
    let retrieved_line = Line::get_by_id(l_id);
    retrieved_line.map(Json).expect("Error loading Line")
}

#[post("/line", format = "json", data = "<line>")]
pub fn create_update(line: Json<Line>) -> Json<Line> {
    let updated_line = Line::update(line);
    updated_line.map(Json).expect("Error updating Line")
}

