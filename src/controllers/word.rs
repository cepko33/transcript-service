use crate::models::Word;
use rocket::serde::json::Json;
use rocket::{delete, get, post};

#[get("/word/<w_id>")]
pub fn get(w_id: i32) -> Json<Word> {
    let retrieved_word = Word::get_by_id(w_id);
    retrieved_word.map(Json).expect("Error loading Word")
}

#[post("/word", format = "json", data = "<word>")]
pub fn create_update(word: Json<Word>) -> Json<Word> {
    let updated_word = Word::update(word);
    updated_word.map(Json).expect("Error updating Word")
}

#[delete("/word/<w_id>")]
pub fn delete(w_id: i32) -> Json<()> {
    let _ = Word::delete(w_id);
    Json(())
}
