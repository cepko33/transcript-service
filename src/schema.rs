// @generated automatically by Diesel CLI.

diesel::table! {
    lines (id) {
        id -> Int4,
        transcript_id -> Int4,
        speaker_id -> Nullable<Int4>,
        attached_photo_url -> Nullable<Text>,
    }
}

diesel::table! {
    shows (id) {
        id -> Int4,
        name -> Text,
        rss_url -> Nullable<Text>,
        description -> Nullable<Text>,
        link -> Nullable<Text>,
        image -> Nullable<Text>,
        author -> Nullable<Text>,
    }
}

diesel::table! {
    speakers (id) {
        id -> Int4,
        name -> Text,
        color -> Text,
    }
}

diesel::table! {
    transcripts (id) {
        id -> Int4,
        title -> Nullable<Varchar>,
        subtitle -> Nullable<Text>,
        description -> Nullable<Text>,
        source_url -> Nullable<Text>,
        header_photo_url -> Nullable<Text>,
        show_id -> Nullable<Int4>,
        episode_number -> Nullable<Int4>,
    }
}

diesel::table! {
    users (id) {
        id -> Int4,
        #[max_length = 100]
        email -> Varchar,
        #[max_length = 122]
        password -> Varchar,
    }
}

diesel::table! {
    words (id) {
        id -> Int4,
        line_id -> Int4,
        content -> Text,
        start_time -> Float8,
        end_time -> Float8,
    }
}

diesel::joinable!(lines -> speakers (speaker_id));
diesel::joinable!(lines -> transcripts (transcript_id));
diesel::joinable!(transcripts -> shows (show_id));
diesel::joinable!(words -> lines (line_id));

diesel::allow_tables_to_appear_in_same_query!(
    lines,
    shows,
    speakers,
    transcripts,
    users,
    words,
);
