#[macro_use]
extern crate rocket;

mod controllers;
mod models;
mod schema;

use diesel::pg::PgConnection;
use diesel::prelude::*;
use dotenvy::dotenv;
use rocket::Build;
use rocket::Rocket;
use std::env;

#[launch]
fn rocket() -> Rocket<Build> {
    rocket::build().mount("/api", controllers::routes::api_routes())
}

pub fn establish_connection() -> PgConnection {
    dotenv().ok();

    let database_url = env::var("DATABASE_URL").expect("DATABASE_URL not set");
    PgConnection::establish(&database_url)
        .unwrap_or_else(|_| panic!("Error connecting to {}", database_url))
}
