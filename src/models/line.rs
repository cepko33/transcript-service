use crate::establish_connection;
use crate::schema::{lines, words};
use diesel::prelude::*;
use rocket::serde::json::Json;
use rocket::serde::{Deserialize, Serialize};

use crate::models::{Speaker, Transcript};
use std::error::Error;

#[derive(
    Queryable, Identifiable, Selectable, Associations, Debug, PartialEq, Serialize, Deserialize,
)]
#[diesel(belongs_to(Transcript))]
#[diesel(belongs_to(Speaker))]
#[diesel(table_name = lines)]
pub struct Line {
    pub id: i32,
    pub transcript_id: i32,
    pub speaker_id: Option<i32>,
    pub attached_photo_url: Option<String>,
}

impl Line {
    pub fn new(
        transcript_id: i32,
        speaker_id: Option<i32>,
    ) -> Result<Line, Box<dyn Error + Send + Sync>> {
        let conn = &mut establish_connection();
        let new_line = diesel::insert_into(lines::table)
            .values((
                lines::transcript_id.eq(transcript_id),
                lines::speaker_id.eq(speaker_id),
            ))
            .returning(Line::as_returning())
            .get_result(conn)?;
        Ok(new_line)
    }

    pub fn get_by_id(line_id: i32) -> Result<Line, Box<dyn Error + Send + Sync>> {
        let conn = &mut establish_connection();
        let selected_line = lines::table
            .filter(lines::id.eq(line_id))
            .select(Line::as_select())
            .get_result(conn)?;

        Ok(selected_line)
    }

    pub fn update(line: Json<Line>) -> Result<Line, Box<dyn Error + Send + Sync>> {
        let conn = &mut establish_connection();
        let updated_line = diesel::update(lines::table)
            .set(lines::speaker_id.eq(&line.speaker_id))
            .filter(lines::id.eq(&line.id))
            .returning(Line::as_returning())
            .get_result(conn)?;

        Ok(updated_line)
    }

    pub fn delete_line_with_words(line_id: i32) -> Result<(), Box<dyn Error + Send + Sync>> {
        let conn = &mut establish_connection();
        diesel::delete(words::table)
            .filter(words::line_id.eq(line_id))
            .execute(conn)?;

        diesel::delete(lines::table)
            .filter(lines::id.eq(line_id))
            .execute(conn)?;

        Ok(())
    }
}
