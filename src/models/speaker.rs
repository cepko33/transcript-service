use crate::establish_connection;
use crate::schema::speakers;
use diesel::prelude::*;
use rocket::serde::json::Json;
use rocket::serde::{Deserialize, Serialize};
use std::error::Error;

#[derive(Queryable, Identifiable, Selectable, Debug, PartialEq, Serialize, Deserialize)]
#[diesel(table_name = speakers)]
pub struct Speaker {
    pub id: i32,
    pub name: String,
    pub color: String,
}

#[derive(Debug, PartialEq, Serialize, Deserialize)]
pub struct SpeakerNew {
    pub name: String,
    pub color: String,
}

impl Speaker {
    pub fn new(name: &str, color: &str) -> Result<Speaker, Box<dyn Error + Send + Sync>> {
        let conn = &mut establish_connection();
        let new_speaker = diesel::insert_into(speakers::table)
            .values((speakers::name.eq(name), speakers::color.eq(color)))
            .returning(Speaker::as_returning())
            .get_result(conn)?;
        Ok(new_speaker)
    }

    pub fn get_by_id(speaker_id: i32) -> Result<Speaker, Box<dyn Error + Send + Sync>> {
        let conn = &mut establish_connection();
        let selected_speaker = speakers::table
            .filter(speakers::id.eq(speaker_id))
            .select(Speaker::as_select())
            .get_result(conn)?;

        Ok(selected_speaker)
    }

    pub fn get_all() -> Result<Vec<Speaker>, Box<dyn Error + Send + Sync>> {
        let conn = &mut establish_connection();
        let selected_speakers = speakers::table
            .select(Speaker::as_select())
            .get_results(conn)?;

        Ok(selected_speakers)
    }

    pub fn update(speaker: Json<Speaker>) -> Result<Speaker, Box<dyn Error + Send + Sync>> {
        let conn = &mut establish_connection();
        let updated_speaker = diesel::update(speakers::table)
            .set(speakers::color.eq(&speaker.color))
            .filter(speakers::id.eq(&speaker.id))
            .returning(Speaker::as_returning())
            .get_result(conn)?;

        Ok(updated_speaker)
    }

    pub fn delete_speaker(s_id: i32) -> Result<(), Box<dyn Error + Send + Sync>> {
        let conn = &mut establish_connection();
        diesel::delete(speakers::table)
            .filter(speakers::id.eq(s_id))
            .execute(conn)?;

        Ok(())
    }
}
