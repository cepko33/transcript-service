use crate::establish_connection;
use crate::schema::shows;
use diesel::prelude::*;
use rocket::serde::{Deserialize, Serialize};
use std::error::Error;

#[derive(Queryable, Identifiable, Selectable, Debug, PartialEq, Serialize, Deserialize)]
pub struct Show {
    pub id: i32,
    pub name: String,
    pub rss_url: Option<String>,
    pub description: Option<String>,
    pub link: Option<String>,
    pub image: Option<String>,
    pub author: Option<String>,
}

#[derive(Insertable, Debug, PartialEq, Serialize, Deserialize)]
#[diesel(table_name = shows)]
pub struct ShowNew {
    pub name: String,
    pub rss_url: Option<String>,
    pub description: Option<String>,
    pub link: Option<String>,
    pub image: Option<String>,
    pub author: Option<String>,
}

impl Show {
    pub fn new(show: ShowNew) -> Result<Show, Box<dyn Error + Send + Sync>> {
        let conn = &mut establish_connection();
        let new_show = diesel::insert_into(shows::table)
            .values(show)
            .returning(Show::as_returning())
            .get_result(conn)?;
        Ok(new_show)
    }
}
