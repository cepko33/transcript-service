use super::user::User;
use chrono::{Duration, Utc};
use hmac::{Hmac, Mac};
use jwt::token::Signed;
use jwt::{Error, Header, SignWithKey, Token, VerifyWithKey};
use rocket::http::Status;
use rocket::outcome::try_outcome;
use rocket::outcome::Outcome::{Error as OutcomeError, Forward, Success};
use rocket::request::{FromRequest, Outcome};
use rocket::Request;
use sha2::Sha256;
use std::collections::BTreeMap;

#[derive(Debug)]
pub struct ApiKey {
    pub sub: String,
    pub exp: i64,
}

#[rocket::async_trait]
impl<'r> FromRequest<'r> for ApiKey {
    type Error = ();

    async fn from_request(request: &'r Request<'_>) -> Outcome<Self, Self::Error> {
        let token_strs: Vec<_> = request.headers().get("Authorization").collect();
        if token_strs.len() != 1 {
            return OutcomeError((Status::Forbidden, ()));
        }

        let mut parsed_header_auth = token_strs[0].split(' ');

        let auth_type = try_outcome!(parsed_header_auth
            .next()
            .map_or_else(|| OutcomeError((Status::BadRequest, ())), Success));

        let token = try_outcome!(parsed_header_auth
            .next()
            .map_or_else(|| OutcomeError((Status::BadRequest, ())), Success));

        if auth_type == "Bearer" {
            match read_token(token) {
                Ok(api_key) => authorize(api_key),
                Err(_) => OutcomeError((Status::Forbidden, ())),
            }
        } else {
            Forward(Status::Unauthorized)
        }
    }
}

fn read_token(token_str: &str) -> Result<ApiKey, Error> {
    let key: Hmac<Sha256> = Hmac::new_from_slice(b"secret")?;
    let verified_token: Result<Token<Header, BTreeMap<String, String>, _>, Error> =
        VerifyWithKey::verify_with_key(token_str, &key);
    match verified_token {
        Ok(token) => Ok(ApiKey {
            sub: token.claims()["sub"].to_string(),
            exp: token.claims()["exp"]
                .parse::<i64>()
                .expect("Failed to convert string to i64"),
        }),
        Err(e) => Err(e),
    }
}

pub fn create_token(
    user: &User,
    header: Header,
) -> Result<Token<Header, BTreeMap<&'static str, String>, Signed>, Error> {
    let expiration = Utc::now()
        .checked_add_signed(Duration::minutes(5))
        .expect("Filed to create timestamp")
        .timestamp();

    let mut claims = BTreeMap::new();
    claims.insert("sub", user.email.to_string());
    claims.insert("exp", expiration.to_string());
    let key: Hmac<Sha256> = Hmac::new_from_slice(b"secret").unwrap();
    let token = Token::new(header, claims);
    token.sign_with_key(&key)
}

fn authorize(api_key: ApiKey) -> Outcome<ApiKey, ()> {
    let is_authorized = api_key.exp > Utc::now().timestamp();
    get_auth_result(is_authorized, api_key)
}

fn get_auth_result(is_authorized: bool, api_key: ApiKey) -> Outcome<ApiKey, ()> {
    match is_authorized {
        true => Success(api_key),
        false => OutcomeError((Status::Forbidden, ())),
    }
}
