use crate::establish_connection;
use crate::schema::users;
use diesel::prelude::*;
use rocket::serde::{Deserialize, Serialize};
use std::error::Error;

use bcrypt::{hash, verify, DEFAULT_COST};

#[derive(Queryable, Debug, Serialize, Deserialize)]
pub struct User {
    pub id: i32,
    pub email: String,
    pub password: String,
}

#[derive(Serialize, Deserialize, Insertable, Debug)]
#[diesel(table_name = users)]
pub struct UserNew {
    pub email: String,
    pub password: String,
}

impl User {
    pub fn new(new_user: UserNew) -> Result<usize, Box<dyn Error + Send + Sync>> {
        let conn = &mut establish_connection();
        let encrypted_user = UserNew {
            password: hash(new_user.password, DEFAULT_COST).unwrap(),
            ..new_user
        };
        Ok(diesel::insert_into(users::table)
            .values(&encrypted_user)
            .execute(conn)?)
    }

    pub fn get_user_with_credentials(email: &str, password: &str) -> Option<User> {
        let conn = &mut establish_connection();
        let result = users::table
            .filter(users::email.eq(email))
            .get_result::<User>(conn);

        match result {
            Ok(user) => match verify(&password, &user.password) {
                Ok(true) => Some(user),
                _ => None,
            },
            Err(_) => None,
        }
    }
}
