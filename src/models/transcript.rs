use crate::establish_connection;
use crate::models::{Line, Speaker, Word};
use crate::schema::{lines, speakers, transcripts, words};
use diesel::prelude::*;
use rocket::serde::Serialize;
use std::error::Error;

#[derive(Queryable, Identifiable, Selectable, Debug, PartialEq, Serialize)]
pub struct Transcript {
    pub id: i32,
    pub title: Option<String>,
    pub subtitle: Option<String>,
    pub description: Option<String>,
    pub source_url: Option<String>,
    pub header_photo_url: Option<String>,
    pub episode_number: Option<i32>,
    pub show_id: Option<i32>,
}

#[derive(Insertable, Debug, PartialEq, Serialize, Default)]
#[diesel(table_name = transcripts)]
pub struct TranscriptNew {
    pub title: Option<String>,
    pub subtitle: Option<String>,
    pub description: Option<String>,
    pub source_url: Option<String>,
    pub header_photo_url: Option<String>,
    pub episode_number: Option<i32>,
    pub show_id: Option<i32>,
}

impl Transcript {
    pub fn get_by_id(transcript_id: i32) -> Result<Transcript, Box<dyn Error + Send + Sync>> {
        let conn = &mut establish_connection();
        let selected_transcript = transcripts::table
            .filter(transcripts::id.eq(transcript_id))
            .select(Transcript::as_select())
            .get_result(conn)?;

        Ok(selected_transcript)
    }

    pub fn get_transcripts() -> Result<Vec<Transcript>, Box<dyn Error + Send + Sync>> {
        let conn = &mut establish_connection();
        let selected_transcripts = transcripts::table
            .select(Transcript::as_select())
            .order(transcripts::id.asc())
            .load(conn)?;

        Ok(selected_transcripts)
    }

    pub fn get_full_transcript(
        transcript_id: i32,
    ) -> Result<(Transcript, Vec<(Line, Vec<Word>)>, Vec<Speaker>), Box<dyn Error + Send + Sync>>
    {
        let conn = &mut establish_connection();
        let selected_transcript = transcripts::table
            .filter(transcripts::id.eq(transcript_id))
            .select(Transcript::as_select())
            .get_result(conn)?;

        let selected_lines = Line::belonging_to(&selected_transcript)
            .select(Line::as_select())
            .order(lines::id.asc())
            .load(conn)?;

        let collected_speaker_ids = selected_lines
            .iter()
            .map(|x| x.speaker_id)
            .filter(|x| x.is_some())
            .map(|x| x.unwrap())
            .collect::<Vec<_>>();

        let selected_speakers = speakers::table
            .filter(speakers::id.eq_any(collected_speaker_ids.iter()))
            .select(Speaker::as_select())
            .get_results(conn)?;

        let selected_words = Word::belonging_to(&selected_lines)
            .select(Word::as_select())
            .order(words::id.asc())
            .load(conn)?;

        let grouped_lines = selected_words
            .grouped_by(&selected_lines)
            .into_iter()
            .zip(selected_lines)
            .map(|(words, line)| (line, words))
            .collect::<Vec<(Line, Vec<Word>)>>();

        Ok((selected_transcript, grouped_lines, selected_speakers))
    }

    pub fn delete_full_transcript(transcript_id: i32) -> Result<(), Box<dyn Error + Send + Sync>> {
        let conn = &mut establish_connection();

        let deleteable_lines = lines::table
            .filter(lines::transcript_id.eq(transcript_id))
            .select(Line::as_select())
            .get_results(conn)?;

        for line in deleteable_lines {
            Line::delete_line_with_words(line.id)?;
        }

        diesel::delete(transcripts::table)
            .filter(transcripts::id.eq(transcript_id))
            .execute(conn)?;

        Ok(())
    }

    pub fn new(transcript: TranscriptNew) -> Result<Transcript, Box<dyn Error + Send + Sync>> {
        let conn = &mut establish_connection();
        let transcript = diesel::insert_into(transcripts::table)
            .values(transcript)
            .returning(Transcript::as_returning())
            .get_result(conn)?;
        Ok(transcript)
    }
}
