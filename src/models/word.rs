use crate::establish_connection;
use crate::schema::words;
use diesel::prelude::*;
use rocket::serde::json::Json;
use rocket::serde::{Deserialize, Serialize};
use std::error::Error;

use crate::models::Line;

#[derive(
    Queryable, Identifiable, Selectable, Associations, Debug, PartialEq, Serialize, Deserialize,
)]
#[diesel(belongs_to(Line))]
#[diesel(table_name = words)]
pub struct Word {
    pub id: i32,
    pub line_id: i32,
    pub content: String,
    pub start_time: f64,
    pub end_time: f64,
}

impl Word {
    pub fn new(
        line_id: i32,
        content: &str,
        start_time: f64,
        end_time: f64,
    ) -> Result<Word, Box<dyn Error + Send + Sync>> {
        let conn = &mut establish_connection();
        let new_word = diesel::insert_into(words::table)
            .values((
                words::line_id.eq(line_id),
                words::content.eq(content),
                words::start_time.eq(start_time),
                words::end_time.eq(end_time),
            ))
            .returning(Word::as_returning())
            .get_result(conn)?;
        Ok(new_word)
    }

    pub fn get_by_id(word_id: i32) -> Result<Word, Box<dyn Error + Send + Sync>> {
        let conn = &mut establish_connection();
        let selected_word = words::table
            .filter(words::id.eq(word_id))
            .select(Word::as_select())
            .get_result(conn)?;

        Ok(selected_word)
    }

    pub fn update(word: Json<Word>) -> Result<Word, Box<dyn Error + Send + Sync>> {
        let conn = &mut establish_connection();
        let updated_word = diesel::update(words::table)
            .set((
                words::content.eq(&word.content),
                words::line_id.eq(&word.line_id),
            ))
            .filter(words::id.eq(&word.id))
            .returning(Word::as_returning())
            .get_result(conn)?;

        Ok(updated_word)
    }

    pub fn delete(w_id: i32) -> Result<(), Box<dyn Error + Send + Sync>> {
        let conn = &mut establish_connection();
        diesel::delete(words::table)
            .filter(words::id.eq(w_id))
            .execute(conn)?;

        Ok(())
    }
}
